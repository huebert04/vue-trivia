import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import GameMenu from './views/GameMenu';
import GamePlay from './views/GamePlay';
import GameOver from './views/GameOver';

Vue.use(VueRouter);
Vue.use(BootstrapVue);

Vue.config.productionTip = false

const routes = [
  {
    path: '/',
    name: 'GameMenu',
    component: GameMenu
  },
  {
    path: '/GamePlay',
    name: 'GamePlay',
    component: GamePlay
  },
  {
    path: '/GameOver',
    name: 'GameOver',
    component: GameOver
  }
  
];

const router = new VueRouter({routes});

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')